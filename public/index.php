<?php

use App\JsonResponseFormatter;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

//определяем рутовую директорию
define('ROOT_DIR', dirname(__DIR__) . '');
require ROOT_DIR . '/vendor/autoload.php';

try {
    //подгружаем .env переменные
    (new Dotenv())->bootEnv(ROOT_DIR . '/.env');

    $container = new ContainerBuilder();
    (new YamlFileLoader($container, new FileLocator([ROOT_DIR])))
        ->load('config/services.yaml');
    //подключение к бд
    $container->set('entityManager', EntityManager::create(
        DriverManager::getConnection([
            'url' => 'mysql://' . $_ENV['MYSQL_USER'] . ':' . $_ENV['MYSQL_PASSWORD'] . '@vi_mysql/vseinstr'
        ]),
        Setup::createAnnotationMetadataConfiguration(
            [
                ROOT_DIR . 'src/Entity/'
            ],
            true,
            null,
            null,
            false
        )
    ));

    $container->compile(true);
    $container->set('container', $container);

    //подключение ядра
    $app = $container->get('kernel');
    $response = $app->handle(Request::createFromGlobals());

} catch (Exception $e) {
    $response = JsonResponseFormatter::internalServerError($e->getMessage());
}

$response->send();

