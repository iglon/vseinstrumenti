<?php

require 'vendor/autoload.php';

use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\Configuration\Migration\YamlFile;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\DBAL\DriverManager;
use Symfony\Component\Dotenv\Dotenv;

define('ROOT_DIR', realpath(__DIR__ . '/..'));
(new Dotenv())->bootEnv(ROOT_DIR . '/.env');

return DependencyFactory::fromConnection(
    new YamlFile('config/migrations.yaml'),
    new ExistingConnection(DriverManager::getConnection([
        'dbname'   => 'vseinstr',
        'user'     => $_ENV['MYSQL_USER'],
        'password' => $_ENV['MYSQL_PASSWORD'],
        'host'     => 'vi_mysql',
        'driver'   => 'pdo_mysql',
    ]))
);