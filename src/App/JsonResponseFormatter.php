<?php

namespace App;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseFormatter {

    /**
     * @param array $data
     * @return JsonResponse
     */
    public static function ok(array $data) : JsonResponse {
        return new JsonResponse([
            'data' => $data,
            'message' => 'OK',
            'code' => Response::HTTP_OK
        ]);
    }

    /**
     * @param string $errorMessage
     * @return JsonResponse
     */
    public static function badRequest(string $errorMessage): JsonResponse {
        return new JsonResponse([
            'data' => [$errorMessage],
            'message' => 'Bad Request',
            'code' => Response::HTTP_BAD_REQUEST
        ]);
    }

    /**
     * @param string $errorMessage
     * @return JsonResponse
     */
    public static function internalServerError(string $errorMessage = 'Unforeseen error'): JsonResponse {
        return new JsonResponse([
            'data' => [$errorMessage],
            'message' => 'Internal Server Error',
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR
        ]);
    }

    /**
     * @return JsonResponse
     */
    public static function notFound(): JsonResponse {
        return new JsonResponse([
            'data' => [],
            'message' => 'Not Found',
            'code' => Response::HTTP_NOT_FOUND
        ]);

    }

}
