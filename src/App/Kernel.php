<?php

namespace App;

use Exception;
use Monolog\Logger;
use Symfony\Component\Routing\RequestContext;
use Throwable;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;

class Kernel {

    /** @var ContainerBuilder */
    private $container;

    /** @var Logger */
    private $logger;

    /**
     * Kernel constructor.
     *
     * @param ContainerBuilder $container
     * @param Logger $logger
     */
    public function __construct(ContainerBuilder $container, Logger $logger) {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handle(Request $request): JsonResponse {
        try {
            [$controllerName, $action] = self::resolveRequest($request);
        } catch (ResourceNotFoundException $e) {
            return JsonResponseFormatter::notFound();
        } catch (MethodNotAllowedException $e) {
            return JsonResponseFormatter::badRequest('Invalid Request type');
        }

        try {
            $this->prepareRequestJsonToArray($request);
        } catch (BadRequestException $e) {
            return JsonResponseFormatter::badRequest($e->getMessage());
        }

        try {
            $controller = $this->container->get($controllerName);
            if (is_null($controller)) {
                throw new ServiceNotFoundException($controllerName);
            }
            if (($controller instanceof Controller) === false) {
                throw new InvalidTypeException('Controller ' . $controllerName . ' is not Controller');
            }
            return $controller->setRequest($request)->$action();
        } catch (BadRequestException $e) {
            return JsonResponseFormatter::badRequest($e->getMessage());
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            return JsonResponseFormatter::internalServerError();
        }

    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function resolveRequest(Request $request): array {
        $router = new Router(
            new YamlFileLoader(new FileLocator([ROOT_DIR . '/config'])),
            'routes.yaml',
            ['cache_dir' => ROOT_DIR . '/var/cache'],
            (new RequestContext())->fromRequest($request)
        );
        return explode('::', $router->matchRequest($request)['_controller']);
    }

    /**
     * @param Request $request
     *
     * @throws BadRequestException
     */
    private function prepareRequestJsonToArray(Request $request): void {
        if (!$request->getContent() || $request->getContentType() !== 'json') {
            return;
        }

        try {
            $content = $request->getContent();
            if (is_resource($content)) {
                throw new BadRequestException('Bad Request');
            }
            $body = json_decode($content, true);
        } catch (Exception $e) {
            throw new BadRequestException('Don\'t valid json ' . $e->getMessage());
        }

        $request->attributes->add(is_array($body) ? $body : []);
        $request->request->replace(is_array($body) ? $body : []);
    }


}