<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;

abstract class Controller {

    /** @var Request $request */
    protected $request;

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request) {
        $this->request = $request;
        return $this;
    }

}