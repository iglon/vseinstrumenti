<?php


namespace App\Factory;

use App\Entity\OrderEntity;
use App\Entity\OrderInfoEntity;
use App\Entity\ProductEntity;

class OrderInfoFactory {

    /**
     * Создание информации по заказу
     *
     * @param OrderEntity $orderEntity
     * @param ProductEntity $productEntity
     * @param int $quantity
     *
     * @return OrderInfoEntity
     */
    public static function create(OrderEntity $orderEntity, ProductEntity $productEntity, int $quantity): OrderInfoEntity {
        return (new OrderInfoEntity())
            ->setOrder($orderEntity)
            ->setProduct($productEntity)
            ->setQuantity($quantity);
    }

}