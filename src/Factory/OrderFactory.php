<?php

namespace App\Factory;

use App\Entity\OrderEntity;
use App\Enum\OrderStatusEnum;

class OrderFactory {

    /**
     * @param int $price
     * @return OrderEntity
     */
    public static function create(int $price): OrderEntity {
        return (new OrderEntity())
            ->setPrice($price)
            ->setStatus(OrderStatusEnum::STATUS_NEW);
    }

}
