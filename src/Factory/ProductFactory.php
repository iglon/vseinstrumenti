<?php

namespace App\Factory;

use App\Entity\ProductEntity;

class ProductFactory {

    /** Создание нового продукта
     *
     * @param string $name
     * @param int $price
     *
     * @return ProductEntity
     */
    public static function create(string $name, int $price): ProductEntity {
        return (new ProductEntity())
        ->setName($name)
        ->setPrice($price);
    }

}