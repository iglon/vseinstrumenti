<?php

namespace App\Enum;

class OrderStatusEnum {

    /** @var string - Статус заказа - новый */
    const STATUS_NEW = 'new';

    /** @var string - Статус заказа - оплачен */
    const STATUS_PAID = 'paid';
}