<?php

namespace App\Controller;

use App\Controller;
use App\JsonResponseFormatter;
use App\Service\ProductService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Exception;

class ProductController extends Controller {

    /** @var ProductService */
    private $productService;

    /**
     * ProductController constructor.
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService) {
        $this->productService = $productService;
    }

    /**
     * @return JsonResponse
     */
    public function generate(): JsonResponse {
        try {
            $products = $this->productService->generate();
            return JsonResponseFormatter::ok($products);
        } catch (Exception $e) {
            return JsonResponseFormatter::internalServerError($e->getMessage());
        }
    }
}