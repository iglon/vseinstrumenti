<?php

namespace App\Controller;

use App\Controller;
use App\Exception\AppServiceBadRequestException;
use App\Exception\ServiceException;
use App\JsonResponseFormatter;
use App\Service\PayService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;

class PayController extends Controller {

    /** @var PayService */
    private $payService;

    /**
     * PayController constructor.
     * @param PayService $payService
     */
    public function __construct(PayService $payService) {
        $this->payService = $payService;
    }

    /**
     * @return JsonResponse
     */
    public function pay(): JsonResponse {
        if (!$this->request->attributes->has('sum')) {
            throw new BadRequestException('Missing parameter "sum"');
        }

        if (!$this->request->attributes->has('orderId')) {
            throw new BadRequestException('Missing parameter "orderId"');
        }

        $sum = $this->request->get('sum');
        $orderId = $this->request->get('orderId');

        if (!is_int($sum) || $sum <= 0) {
            throw new BadRequestException('Invalid sum - ' . $sum);
        }

        if (!is_int($orderId) || $orderId <= 0) {
            throw new BadRequestException('Invalid orderId - ' . $orderId);
        }

        try {
            $this->payService->payOrder($sum, $orderId);
            return JsonResponseFormatter::ok([]);
        } catch (AppServiceBadRequestException $e) {
            return JsonResponseFormatter::internalServerError($e->getMessage());
        } catch (ServiceException $e) {
            return JsonResponseFormatter::internalServerError($e->getMessage());
        }
    }

}