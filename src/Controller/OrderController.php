<?php

namespace App\Controller;

use App\Controller;
use App\Exception\ServiceException;
use App\JsonResponseFormatter;
use App\Service\OrderService;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;

class OrderController extends Controller {

    /** @var OrderService */
    private $orderService;

    /**
     * OrderController constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService) {
        $this->orderService = $orderService;
    }

    /**
     * Создание заказа
     *
     * @return JsonResponse
     */
    public function create(): JsonResponse {

        if (!$this->request->attributes->has('products')) {
            throw new BadRequestException('Missing parameter "products"');
        }

        $products = $this->request->attributes->get('products');

        foreach ($products as $productId => $quantity) {

            if (!is_int($productId) || $productId < 0) {
                throw new BadRequestException('Invalid id product - ' . $productId);
            }

            if (!is_int($quantity) || $quantity < 0) {
                throw new BadRequestException('Invalid quantity products -  ' . $quantity);
            }
        }

        try {
            return JsonResponseFormatter::ok([
                'orderId' => $this->orderService->create($products)
            ]);
        } catch (ServiceException $e) {
            return JsonResponseFormatter::badRequest($e->getMessage());
        }
    }

}