<?php

namespace App\Service;

use App\Entity\OrderEntity;
use App\Enum\OrderStatusEnum;
use App\Exception\{AppServiceBadRequestException, RepositoryException, ServiceException};
use App\Repository\OrderRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Monolog\Logger;

class PayService {

    /** @var Logger */
    private $logger;

    /** @var OrderRepository */
    private $orderRepository;

    /** @var Client */
    private $client;

    /** @var OrderEntity */
    private $orderEntity;

    /**
     * PayService constructor.
     * @param OrderRepository $orderRepository
     * @param OrderEntity $orderEntity
     * @param Client $client
     * @param Logger $logger
     */
    public function __construct(OrderRepository $orderRepository, OrderEntity $orderEntity, Client $client, Logger $logger) {
        $this->orderRepository = $orderRepository;
        $this->orderEntity = $orderEntity;
        $this->client = $client;
        $this->logger = $logger;
    }

    /**
     * @param int $sum
     * @param int $orderId
     * @return array
     * @throws AppServiceBadRequestException
     */
    public function payOrder(int $sum, int $orderId): array {

        try {
            $order = $this->check($sum, $orderId);
        } catch (BadRequestException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceBadRequestException($e->getMessage());
        } catch (ServiceException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceBadRequestException($e->getMessage());
        }

        try {
            $this->pay($sum);
        } catch (ServiceException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceBadRequestException($e->getMessage());
        }

        try {
            $this->orderRepository->save($order->setStatus(OrderStatusEnum::STATUS_PAID));
        } catch (RepositoryException $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            throw new AppServiceBadRequestException($e->getMessage());
        }

        return (array)$order;
    }

    /**
     * Проверка заказа и суммы денег
     *
     * @param int $sum
     * @param int $orderId
     *
     * @return OrderEntity
     * @throws ServiceException
     * @throws BadRequestException
     */
    public function check(int $sum, int $orderId): OrderEntity {
        $order = $this->orderRepository->findById($orderId);

        if (is_null($order)) {
            throw new BadRequestException('Not found order with id - ' . $orderId);
        }

        if ($order->getPrice() != $sum) {
            throw new BadRequestException('Order amount does not match - ' . $sum);
        }

        if ($order->getStatus() !== OrderStatusEnum::STATUS_NEW) {
            throw new ServiceException('The order cannot be paid - ' . $orderId);
        }

        return $order;
    }

    /**
     * Оплата заказа (имитация платежа)
     *
     * @param int $sum
     * @throws ServiceException
     */
    public function pay(int $sum) {
        try {
            $response = $this->client->get('http://ya.ru/');
            if ($response->getStatusCode() !== Response::HTTP_OK) {
                throw new ServiceException('Error pay');
            }
        } catch (GuzzleException $e) {
            throw new ServiceException('Error pay - ' . $e->getMessage());
        }
    }

}
