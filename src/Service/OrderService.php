<?php

namespace App\Service;

use App\Exception\ServiceException;
use App\Factory\OrderFactory;
use App\Transaction\OrderTransaction;
use App\Repository\ProductRepository;
use Monolog\Logger;

class OrderService {

    /** @var OrderFactory */
    private $orderFactory;

    /** @var Logger */
    private $logger;

    /** @var ProductRepository */
    private $productRepository;

    /** @var OrderTransaction */
    private $orderTransaction;

    /**
     * OrderService constructor.
     * @param OrderFactory $orderFactory
     * @param ProductRepository $productRepository
     * @param OrderTransaction $orderTransaction
     * @param Logger $logger
     */
    public function __construct(
        OrderFactory $orderFactory,
        ProductRepository $productRepository,
        OrderTransaction $orderTransaction,
        Logger $logger
    ) {
        $this->orderFactory = $orderFactory;
        $this->productRepository = $productRepository;
        $this->orderTransaction = $orderTransaction;
        $this->logger = $logger;
    }

    /**
     * @param array $products
     * @return int
     * @throws ServiceException
     */
    public function create(array $products): int {
        $price = 0;
        foreach ($products as $idProduct => $quantity) {
            $product = $this->productRepository->findById($idProduct);
            if (is_null($product)) {
                throw new ServiceException('This product with id = ' . $idProduct . ' not found!');
            }
            $price += $product->getPrice() * $quantity;
        }
        $order = $this->orderTransaction->createOrderTransaction($products, $price);

        return $order->getId();
    }
}
