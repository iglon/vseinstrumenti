<?php

namespace App\Service;

use App\Entity\ProductEntity;
use App\Exception\ServiceException;
use App\Factory\ProductFactory;
use App\Repository\ProductRepository;
use Monolog\Logger;
use Exception;

class ProductService {

    /** @var int - кол-во продуктов для генерации */
    private static $countProducts = 20;

    /** @var ProductEntity */
    private $productEntity;

    /** @var ProductRepository  */
    private $productRepository;

    /** @var Logger */
    private $logger;

    /**
     * ProductService constructor.
     * @param ProductEntity $productEntity
     * @param ProductRepository $productRepository
     * @param Logger $logger
     */
    public function __construct(ProductEntity $productEntity, ProductRepository $productRepository, Logger $logger) {
        $this->productEntity = $productEntity;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
    }

    /**
     *  Генерация товаров
     *
     * @return array
     * @throws ServiceException
     */
    public function generate(): array {
        try {
            $products = array_map(
                function () {
                    $product = ProductFactory::create(
                        str_shuffle('abcdefghijklmnopqrstuvwxyz'),
                        random_int(10, 1000)
                    );
                    return $this->productRepository->save($product)->getId();
                },
                range(1, self::$countProducts)
            );
        } catch (Exception $e) {
            $this->logger->error('Error generate products - ' . $e->getMessage());
            throw new ServiceException($e->getMessage());
        }

        return $products;
    }
}
