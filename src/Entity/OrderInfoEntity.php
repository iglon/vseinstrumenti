<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="orders_info")
 */
class OrderInfoEntity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ProductEntity", cascade={"detach"})
     * @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     *
     * @var ProductEntity
     */
    private $productId;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntity", inversedBy="products")
     * @ORM\JoinColumn(name="id_order", referencedColumnName="id")
     *
     * @var OrderEntity
     */
    private $orderId;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity(int $quantity): self {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @param ProductEntity $productEntity
     * @return $this
     */
    public function setProduct(ProductEntity $productEntity): self {
        $this->productId = $productEntity;
        return $this;
    }

    /**
     * @param OrderEntity $orderEntity
     * @return $this
     */
    public function setOrder(OrderEntity $orderEntity): self {
        $this->orderId = $orderEntity;
        return $this;
    }

}
