<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201030202216 extends AbstractMigration
{

    public function up(Schema $schema) : void
    {
        $this->addSql(
            'CREATE TABLE orders_info(
                id_order INT NOT NULL,
                id_product INT NOT NULL,
                quantity INT NOT NULL,
                PRIMARY KEY (id_order, id_product)
            )'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE order_payment');
    }
}
