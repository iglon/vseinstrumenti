<?php

namespace App\Transaction;

use App\Entity\OrderEntity;
use App\Entity\OrderInfoEntity;
use App\Entity\ProductEntity;
use App\Exception\ServiceException;
use App\Factory\OrderFactory;
use App\Factory\OrderInfoFactory;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Exception;

class OrderTransaction {

    /** @var EntityManager */
    private $entityManager;

    /** @var OrderFactory */
    private $orderFactory;

    /** @var OrderInfoFactory */
    private $orderInfoFactory;

    /** @var ProductRepository */
    private $productRepository;

    /**
     * OrderTransaction constructor.
     * @param OrderFactory $orderFactory
     * @param OrderInfoFactory $orderInfoFactory
     * @param ProductRepository $productRepository
     * @param EntityManager $entityManager
     */
    public function __construct(
        OrderFactory $orderFactory,
        OrderInfoFactory $orderInfoFactory,
        ProductRepository $productRepository,
        EntityManager $entityManager
    ) {
        $this->orderFactory = $orderFactory;
        $this->orderInfoFactory = $orderInfoFactory;
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * Транзакция для создания заказа
     *
     * @param array $products
     * @param int $price
     *
     * @return OrderEntity
     * @throws ServiceException
     */
    public function createOrderTransaction(array $products, int $price): OrderEntity {
        try {
            $this->entityManager->getConnection()->beginTransaction();
            $order = $this->createOrder($price);
            foreach ($products as $idProduct => $quantity) {
                $product = $this->productRepository->findById($idProduct);
                $this->entityManager->persist($this->createInfoOrder($order, $product, $quantity));
            }
            $this->entityManager->persist($order);
            $this->entityManager->flush();
            $this->entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $this->entityManager->rollback();
            throw new ServiceException($e->getMessage());
        }
        return $order;
    }

    /**
     * Создание заказа
     *
     * @param int $price
     * @return OrderEntity
     */
    public function createOrder(int $price): OrderEntity {
       return $this->orderFactory->create($price);
    }

    /**
     * Создание записи товаров к заказу
     *
     * @param OrderEntity $order
     * @param ProductEntity $productEntity
     * @param int $quantity
     *
     * @return OrderInfoEntity
     */
    public function createInfoOrder(OrderEntity $order, ProductEntity $productEntity, int $quantity): OrderInfoEntity {
        return $this->orderInfoFactory->create($order, $productEntity, $quantity);
    }
}
