<?php

namespace App\Repository;

use App\Entity\OrderEntity;
use App\Exception\RepositoryException;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

class OrderRepository {

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ProductRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param OrderEntity $order
     * @return OrderEntity
     * @throws RepositoryException
     */
    public function save(OrderEntity $order): OrderEntity {
        try {
            $this->entityManager->persist($order);
            $this->entityManager->flush();
            return $order;
        } catch (Throwable $e) {
            throw new RepositoryException('Order - ' . $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id): ?OrderEntity {
        return $this->entityManager->find(OrderEntity::class,  $id);
    }

}