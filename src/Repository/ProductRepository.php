<?php

namespace App\Repository;

use App\Entity\ProductEntity;
use App\Exception\RepositoryException;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

class ProductRepository {

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ProductsRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ProductEntity $product
     * @return ProductEntity
     * @throws RepositoryException
     */
    public function save(ProductEntity $product): ProductEntity {
        try {
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            return $product;
        } catch (Throwable $e) {
            throw new RepositoryException('Order - ' . $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id): ?ProductEntity {
        return $this->entityManager->find(ProductEntity::class,  $id);
    }

}